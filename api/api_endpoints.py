import datetime

from connexion import NoContent

documents = {}


def post(body):
    count = len(documents)

    document = {'id': count + 1,
                'Name': body.get('Name'),
                'Tag': body.get('Tag'),
                'Last_updated': datetime.datetime.now()}

    documents[document['id']] = document

    return document, 201


def put(body):

    _id = body.get('id')

    document = documents.get(_id, {'id': _id})

    document['Name'] = body.get['Name']
    document['Tag'] = body.get['Tag']
    document['Last_updated'] = datetime.datetime.now()

    documents[_id] = document

    return document[_id]


def delete(_id):

    _id = int(_id)
    if not documents.get(_id):
        return NoContent, 404
    del documents[_id]

    return NoContent, 204


def get(documentID):
    _id = int(documentID)
    if not documents.get(_id):
        return NoContent, 404

    return documents[_id]


def search(limit=100):

    return list(documents.values())[0:limit]
