import connexion
from connexion.resolver import RestyResolver
from connexion.decorators.uri_parsing import OpenAPIURIParser


app = connexion.FlaskApp(__name__, specification_dir='api/')

options = {'uri_parsing_class': OpenAPIURIParser}


if __name__ == '__main__':
    app.add_api('api.yaml',
                arguments={'title': 'Resty_resolver'},
                resolver=RestyResolver('api'),)

    app.run(port=8080)
